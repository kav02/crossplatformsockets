
#include "StringUtils.h"
#include "SocketAddress.h"
#include "SocketAddressFactory.h"
#include "UDPSocket.h"
#include "TCPSocket.h"
#include "SocketUtil.h"

#include <iostream>

void echoServer();
void realEchoServer();

#if _WIN32
int WINAPI WinMain( _In_ HINSTANCE hInstance, _In_opt_ HINSTANCE hPrevInstance, _In_ LPSTR lpCmdLine, _In_ int nCmdShow )
{
	UNREFERENCED_PARAMETER( hPrevInstance );
	UNREFERENCED_PARAMETER( lpCmdLine );

  echoServer();
//  realEchoServer();
}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

//  echoServer();
  realEchoServer();
}
#endif

void echoServer()
{
  TCPSocketPtr server;

  SocketUtil::StaticInit();

  server = SocketUtil::CreateTCPSocket(SocketAddressFamily::INET);

  // Create a new socket address
  const SocketAddressPtr socketAddress = SocketAddressFactory::CreateIPv4FromString("0.0.0.0:654321");

  // Bind to it.
  server->Bind(*socketAddress);

  // Start listening (backlog 5)
  server->Listen(5);

  // Create a socket to store the information from
  // the other end.
  SocketAddressPtr otherAddress(new SocketAddress());

  bool runFlag = true;

  while(runFlag == true)
  {
    // Accept a connection - note connect blocks.
    TCPSocketPtr client = server->Accept(*otherAddress);

    if(otherAddress != nullptr)
    {
      //TODO add some support functions to get these printed in a better format.
      std::cout << "Address (not string format):" << otherAddress->getIP4() << std::endl;
      std::cout << "Port (not string format):" << otherAddress->getPort() << std::endl;
      std::cout << "Family (not string format):" << otherAddress->getFamily() << std::endl;
    }

    // Read something to 'echo' back to the client.
    const int BUFF_MAX = 32;
    char buff[BUFF_MAX];

    int readCount = 0;
    int totalRead = 0;
    do
    {
      /* If we loose the CPU (block) we'll retun from the Receive, need to
      keep reading until we've go the whole message */
      readCount = client->Receive(static_cast<void*>(buff),BUFF_MAX);
      totalRead += readCount;
    } while(readCount > 0);

    // EOF (end of file/stream) and string null terminator are the same.
    // so this gets stripped off.
    buff[totalRead] = 0;

    string recStr(buff);

    std::cout << "Server has recieved:" << recStr << std::endl;

    client->Send(static_cast<const void*>(recStr.c_str()),recStr.length()+1);

    if(recStr.compare(0,4,"exit") == 0)
    {
      std::cout << "Told to quit!" << std::endl;
      runFlag = false;
    }
  }
  //Note: The smart pointers will decrement their reference
  // count when they're scope (function/class they were created in)
  // ends.

  std::cout << "TCPSocketPtr - shared ptr use count - pre-reset" << server.use_count() << std::endl;
  server.reset();
  std::cout << "TCPSocketPtr - shared ptr use count - post-reset" << server.use_count() << std::endl;

  SocketUtil::CleanUp();
}

void realEchoServer()
{
  TCPSocketPtr server;

  SocketUtil::StaticInit();

  server = SocketUtil::CreateTCPSocket(SocketAddressFamily::INET);

  // Create a new socket address
  const SocketAddressPtr socketAddress = SocketAddressFactory::CreateIPv4FromString("0.0.0.0:654321");

  // Bind to it.
  server->Bind(*socketAddress);

  // Start listening (backlog 5)
  server->Listen(5);

  // Create a socket to store the information from
  // the other end.
  SocketAddressPtr otherAddress(new SocketAddress());

  // Accept a connection - note connect blocks.
  TCPSocketPtr client = server->Accept(*otherAddress);

  // Read something to 'echo' back to the client.
  const int BUFF_MAX = 32;
  char buff[BUFF_MAX];

  int readCount = 0;

  do
  {
    /* If we loose the CPU (block) we'll retun from the Receive, need to
    keep reading until we've go the whole message */
    readCount = client->Receive(static_cast<void*>(buff),1);
    if(readCount > 0)
      client->Send(static_cast<const void*>(buff),readCount);
  } while(readCount > 0);

  SocketUtil::CleanUp();
}
